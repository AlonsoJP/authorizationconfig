package br.com.server.authorizationconfig.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.server.authorizationconfig.entity.UserEntity;
import br.com.server.authorizationconfig.repository.UserRepository;

@Service
public class AppUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

		Optional<UserEntity> userOptional = userRepository.findByEmail(email);

		UserEntity userResource = userOptional
				.orElseThrow(() -> new UsernameNotFoundException("Usario e/ou senha inválido!"));

		return new User(email, userResource.getPassword(), getPermissions(userResource));
	}

	private Collection<? extends GrantedAuthority> getPermissions(UserEntity userResource) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		userResource.getPermission()
				.forEach(p -> authorities.add(new SimpleGrantedAuthority(p.getDescription().toUpperCase())));

		return authorities;
	}

}
