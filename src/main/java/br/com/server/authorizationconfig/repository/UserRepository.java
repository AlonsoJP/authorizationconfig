package br.com.server.authorizationconfig.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.server.authorizationconfig.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

	public Optional<UserEntity> findByEmail(String email);

}
