package br.com.server.authorizationconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;


@SpringBootApplication
@EnableAuthorizationServer
public class AuthorizationconfigApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(AuthorizationconfigApplication.class, args);
	}

}
